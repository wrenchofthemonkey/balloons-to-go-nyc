<?php
global $theme_sidebars;
$places = array();
foreach ($theme_sidebars as $sidebar){
    if ($sidebar['group'] !== 'footer')
        continue;
    $widgets = theme_get_dynamic_sidebar_data($sidebar['id']);
    if (!is_array($widgets) || count($widgets) < 1)
        continue;
    $places[$sidebar['id']] = $widgets;
}
$place_count = count($places);
$needLayout = ($place_count > 1);
if (theme_get_option('theme_override_default_footer_content')) {
    if ($place_count > 0) {
        $centred_begin = '<div class="art-center-wrapper"><div class="art-center-inner">';
        $centred_end = '</div></div><div class="clearfix"> </div>';
        if ($needLayout) { ?>
<div class="art-content-layout">
    <div class="art-content-layout-row">
        <?php 
        }
        foreach ($places as $widgets) { 
            if ($needLayout) { ?>
            <div class="art-layout-cell art-layout-cell-size<?php echo $place_count; ?>">
            <?php 
            }
            $centred = false;
            foreach ($widgets as $widget) {
                 $is_simple = ('simple' == $widget['style']);
                 if ($is_simple) {
                     $widget['class'] = implode(' ', array_merge(explode(' ', theme_get_array_value($widget, 'class', '')), array('art-footer-text')));
                 }
                 if (false === $centred && $is_simple) {
                     $centred = true;
                     echo $centred_begin;
                 }
                 if (true === $centred && !$is_simple) {
                     $centred = false;
                     echo $centred_end;
                 }
                 theme_print_widget($widget);
            } 
            if (true === $centred) {
                echo $centred_end;
            }
            if ($needLayout) {
           ?>
            </div>
        <?php 
            }
        } 
        if ($needLayout) { ?>
    </div>
</div>
        <?php 
        }
    }
?>
<div class="art-footer-text">
<?php
global $theme_default_options;
echo do_shortcode(theme_get_option('theme_override_default_footer_content') ? theme_get_option('theme_footer_content') : theme_get_array_value($theme_default_options, 'theme_footer_content'));
} else { 
?>
<div class="art-footer-text">
<?php theme_ob_start() ?>
  
<div class="art-content-layout">
    <div class="art-content-layout-row">
    <div class="art-layout-cell" style="width: 33%"><?php if (false === theme_print_sidebar('footer-1-widget-area')) { ?>
        <p><fid1 itemscope="" itemtype="https://schema.org/LocalBusiness">
                                                                                                                                                        </fid1></p><h2 style="text-align: left;">
                                                                                                                                                        <span style="text-shadow: #171717 1px 0px 0px, #171717 -1px 0px 0px, #171717 0px -1px 0px, #171717 0px 1px 0px, rgba(0, 0, 0, 0.984375) 0px 0px 10px; color: #FFFFFF;"><br /></span></h2>
                                                                                                                                                        <h2 itemprop="about" style="text-align: center;"><span style="text-shadow: #171717 1px 0px 0px, #171717 -1px 0px 0px, #171717 0px -1px 0px, #171717 0px 1px 0px, rgba(0, 0, 0, 0.984375) 0px 0px 10px; color: #FFFFFF;">Products List</span></h2>
                                                                                                                                                        <p style="text-align: left;"><span style="text-shadow: #171717 1px 0px 0px, #171717 -1px 0px 0px, #171717 0px -1px 0px, #171717 0px 1px 0px, rgba(0, 0, 0, 0.984375) 0px 0px 10px; color: #FFFFFF;"><br /></span></p><h5 style="text-align: left;">
                                                                                                                                                        <img itemprop="image" width="180" height="191" alt="" src="<?php echo get_template_directory_uri() ?>/images/Products%20-%20Balloons%20To%20Go%20Left.png" style="float: right;" class="" /><span style="font-weight: bold;">&gt;</span> 
                                                                                                                                                        <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/products/arches']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Arches by Balloons To Go">Arches</a><br /></h5>
                                                                                                                                                        <h5 style="text-align: left;">&gt; <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/products/centerpieces']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Centerpieces by Balloons To Go">Centerpieces</a></h5>
                                                                                                                                                        <h5 style="text-align: left;">&gt; <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/products/color-chart']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Color Chart by Balloons To Go">Color Chart</a></h5>
                                                                                                                                                        <h5 style="text-align: left;">&gt; <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/products/columns']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Columns by Balloons To Go">Columns</a>&nbsp;&nbsp;</h5>
                                                                                                                                                        <h5 style="text-align: left;">&gt; <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/products/custom-imprints']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Custom Imprints by Balloons To Go">Custom Imprints</a></h5>
                                                                                                                                                        <h5 style="text-align: left;">&gt; <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/products/decorating']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Decorating by Balloons To Go">Decorating</a></h5>
                                                                                                                                                        <h5 style="text-align: left;">&gt; <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/products/sculptures']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Sculptures by Balloons To Go">Sculptures</a></h5>
                                                                                                                                                        <h5 style="text-align: left;">&gt; <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/products/something-different']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Something Diferent by Balloons To Go">Something Different</a>&nbsp;</h5><br /><p>
                                                                                                                                                        </p><p>
                                                                                                                                            </p>
    <?php } ?></div><div class="art-layout-cell" style="width: 34%"><?php if (false === theme_print_sidebar('footer-2-widget-area')) { ?>
        <p></p><h2 itemprop="about"><br /><span style="text-shadow: #171717 1px 0px 0px, #171717 -1px 0px 0px, #171717 0px -1px 0px, #171717 0px 1px 0px, rgba(0, 0, 0, 0.984375) 0px 0px 10px; color: #FFFFFF;">Hours of Operations</span></h2>
                                                                                                                                                        <br /><fid2 itemscope="" itemtype="https://schema.org/LocalBusiness">
                                                                                                                                                        <time itemprop="openingHours" datetime="Mo, Tu, We, Th, Fr 10:00-18:00">
                                                                                                                                                        <p>&nbsp;<span style="color: rgb(255, 255, 255);">Monday – Friday : 10am- 6pm</span></p><p>
                                                                                                                                                        <time itemprop="openingHours" datetime="Sa 10:00-15:00">
                                                                                                                                                        </time></p><p><span style="color: rgb(255, 255, 255);">Saturday: Office hours 10am- 3pm &amp;<spec itemprop="openingHoursSpecification"> Pre-arranged deliveries</spec></span></p>
                                                                                                                                                        <time itemprop="openingHours" datetime="Su 11:00-14:00">
                                                                                                                                                        <p><span style="color: rgb(255, 255, 255);">Sunday: Office Hours 11am- 2pm &amp; <spec itemprop="openingHoursSpecification"> Pre-arranged deliveries</spec></span></p><p>
                                                                                                                                                        </p><p><br /></p>
                                                                                                                                                        <h3><span style="color: rgb(255, 255, 255); text-shadow: rgb(23, 23, 23) 1px 0px 0px, rgb(23, 23, 23) -1px 0px 0px, rgb(23, 23, 23) 0px -1px 0px, rgb(23, 23, 23) 0px 1px 0px, rgba(0, 0, 0, 0.984375) 0px 0px 10px;"><payment itemprop="paymentAccepted">All Major credit cards and company checks are accepted.</payment></span></h3>
                                                                                                                                                        <p><br /></p><p><br /></p></time></time></fid2>
                                                                                                                                                        <p><span style="color: #FFFFFF;">Copyright © <cr itemprop="copyrightYear">1981</cr>. All Rights Reserved.</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                                                                                                                                        <p>&nbsp;<a itemprop="publishingPrinciples" href="<?php theme_ob_start() ?>[post_link name='/privacy-policy']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_blank" title="Balloons To Go - Privacy Policy">Privacy Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a itemprop="publishingPrinciples" href="<?php theme_ob_start() ?>[post_link name='/term-of-use']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_blank" title="Balloons To Go - Terms of Use">Terms Of Use</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/about-us']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_self" title="Balloons To Go - About Us">About Us</a></p>&nbsp;<?php theme_ob_start() ?><a itemprop="url" href="http://feeds.feedburner.com/balloonstogonyc" target="_blank" title="Balloons To Go RSS Feed" class="art-rss-tag-icon" style="line-height: 9px; "></a><?php echo do_shortcode(theme_ob_get_clean()) ?>&nbsp;
                                                                                                                                                        <p>
                                                                                                                                            </p>
    <?php } ?></div><div class="art-layout-cell" style="width: 33%"><?php if (false === theme_print_sidebar('footer-3-widget-area')) { ?>
        <p></p><p>
                                                                                                                                                        </p><p><fid3 itemscope="" itemtype="https://schema.org/LocalBusiness"><img itemprop="logo" style="margin-right: 4px;" src="https://lh5.googleusercontent.com/-srLxLeJXLP8/U1iYEWr2isI/AAAAAAAAANE/II1BWjI_52w/w159-h139-no/Balloons+To+Go+Logo.png" alt="Balloons To Go" width="139" height="139" class="" /><br />
                                                                                                                                                        </p><div itemscope="" itemtype="https://schema.org/LocalBusiness" id="" style="text-align: center;">
                                                                                                                                                        <div itemprop="legalName"><h2><span style="text-shadow: #171717 1px 0px 0px, #171717 -1px 0px 0px, #171717 0px -1px 0px, #171717 0px 1px 0px, rgba(0, 0, 0, 0.984375) 0px 0px 10px; color: #FFFFFF;">Balloons To Go</span></h2></div>
                                                                                                                                                         <a itemprop="email" href="mailto:contact@balloonstogonyc.com" title="Write Us">contact@balloonstogonyc.com</a>
                                                                                                                                                        <div itemprop="address">
                                                                                                                                                        <div itemprop="map"><a href="http://goo.gl/maps/eeM9b" target="_blank" title="Directions">236 West 15th Street Basement East
                                                                                                                                                         New York
                                                                                                                                                         , 
                                                                                                                                                         NY
                                                                                                                                                         , 
                                                                                                                                                         10011
                                                                                                                                                         
                                                                                                                                                         USA</a></div><span></span></addy>
                                                                                                                                                         
                                                                                                                                                        </div>
                                                                                                                                                        <div itemprop="telephone"><a href="tel://1-212-989-9338" title="Call Us">(212) 989-9338</a></div>
                                                                                                                                                        <div><span itemprop="description" class="category" style="color: #FFFFFF;">Local Business</span><span style="color: #FFFFFF;">,</span> <span itemprop="description" class="category" style="color: #FFFFFF;">Balloon Store</span></div></div></fid3>
                                                                                                                                                        <p></p><p>
                                                                                                                                            </p>
    <?php } ?></div>
    </div>
</div>

    

<?php echo do_shortcode(theme_ob_get_clean()) ?>
<?php } ?>
<p class="art-page-footer">
        <span id="art-footnote-links">Designed by <a href="http://www.wrenchofthemonkey.com" target="_blank">Wrench of the Monkey</a>.</span>
    </p>
</div>